# Advent of Code 2020

This repository contains my solutions for the Advent of Code's 2020 problems.
See `docs/challenges.md` for notes about the individual challenges.

## Building

This project uses the [Meson](https://mesonbuild.com/) build system. To build
the code, use the following commands:

``` shell
meson setup build/
ninja -C build/ all
```
