# Notes about the Challenges

This doc contains my notes related to the challenges. After building the code,
each challenge can be solved via the command line invocations below.

## Challenge 1

I came up with a general solution that will search a file for `N` numbers that
sum to *2020* and will print out their product. It uses a depth first search
approach to consider every combination of `N` numbers from the array.

```shell
./build/src/challenges/01 --input ./src/data/01.txt --num_entries 2
./build/src/challenges/01 --input ./src/data/01.txt --num_entries 3
```

## Challenge 2

The fun part of this challenge was handling complex parsing into a structure.
C++ provides powerful stream operators to handle this; I can define `operator<<`
on my `struct` to allow parsing it from any stream.

The text format of the record can be represented by the following pattern:
`%d-%d %c: %s\n`. There are two characters present in the text format that are
not data: `-` and `:`. To handle parsing of these extra characters, I created
the following `struct`:

```c++
struct ExpectedChar {
  char expected;

  friend std::istream& operator>>(std::istream& in, ExpectedChar& e) {
    char c;
    if (in >> c && c != e.expected) {
      in.setstate(std::ios::failbit);
    }
    return in;
  }
};
```

The `ExpectedChar` will parse exactly one character from the input stream and
compare it to an expected value, and if the values do not match, then it will
set the failure bit in the input stream.

```shell
./build/src/challenges/02 --input ./src/data/02.txt --policy count
./build/src/challenges/02 --input ./src/data/02.txt --policy xor
```

## Challenge 3

I stored the map as a 2-d array of integers, with 0 representing an empty space
and 1 representing a tree. The total number of trees encountered is the sum of
the positions in the map checked. The secret to handle the "extension" of the
map is to use modular arithmetic for the horizontal positioning.

```shell
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 3 --vertical 1
# Use a calculator to determine the product of these 5 invocations
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 1 --vertical 1
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 3 --vertical 1
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 5 --vertical 1
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 7 --vertical 1
./build/src/challenges/02 --input ./src/data/02.txt --horizontal 1 --vertical 2
```

## Challenge 4

This is the first challenge to contain multiline input. To process this, I used
`std::getline`  to store string values in a struct. I then needed to write
complex validation functions in order to validate each item. Abseil comes in
handy here: `absl/strings/numbers.h` provided conversions from strings to
integers and `absl/algorithm/container.h` provided `c_none_of` to determine if
an input did not match a list of valid values. The standard library also
provides helpful utilities: `std::isdigit` and `std::isxdigit` check numbers and
hexadecimal digits, and `std::string_view::remove_prefix` allows parsing complex
fields piece by piece.

```shell
./build/src/challenges/04 --input ./src/data/04.txt
./build/src/challenges/04 --input ./src/data/04.txt --validateFields
```

## Challenge 5

Each seat can be transformed into a seat ID by interpreting the ticket as a bit
string where `B` and `R` are treated as `1` and `F` and `L` are treated as `0`.
The string can be converted to a number using a `std::bitset`. Parse all the
seats, store them in a `std::vector`, and return the max and missing elements.

```shell
./build/src/challenges/05 --input ./src/data/05.txt
```

## Challenge 6

This challenge uses similar processing to challenge 4; except this time, it's
possible to maintain an array of 26 integers tracking the number of questions
that were answered. I also store the number of members in a group to calculate
using the correct policy.

```shell
./build/src/challenges/06 --input ./src/data/06.txt --policy any
./build/src/challenges/06 --input ./src/data/06.txt --policy all
```
