#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "absl/flags/flag.h"
#include "absl/logging/log_container.h"
#include "absl/logging/logging.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");
ABSL_FLAG(int, num_entries, 2, "the number of entries to sum to 2020");

void select_combinations_helper(const std::vector<int>& entries,
                                std::vector<int>& combo, int offset, int num) {
  LOG(INFO) << "Testing combo " << absl::LogContainer(combo) << " offset "
            << offset << " remaining " << num;
  if (num == 0) {
    int sum = 0;
    int product = 1;
    for (int entry : combo) {
      sum += entry;
      product *= entry;
    }
    if (sum == 2020) {
      std::cout << product << std::endl;
    }
    return;
  }
  for (int i = offset; i < entries.size() - num; ++i) {
    combo.push_back(entries[i]);
    select_combinations_helper(entries, combo, i + 1, num - 1);
    combo.pop_back();
  }
}

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day 1", argc, argv);

  std::vector<int> entries;
  int temp_entry;
  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  while (file_stream >> temp_entry) {
    entries.push_back(temp_entry);
  }

  LOG(INFO) << "Read " << entries.size() << " entries";

  std::vector<int> combo;
  select_combinations_helper(entries, combo, 0,
                             absl::GetFlag(FLAGS_num_entries));

  return 0;
}
