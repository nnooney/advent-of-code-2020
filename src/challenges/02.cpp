#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "absl/flags/flag.h"
#include "absl/logging/logging.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");
ABSL_FLAG(std::string, policy, "count", "policy for passwords (count, xor)");

// A struct that validates parsing an expected char from an input stream and
// discards it. If the character doesn't match the expected value, then the
// input stream's failbit is set.
struct ExpectedChar {
  char expected;

  friend std::istream& operator>>(std::istream& in, ExpectedChar& e) {
    char c;
    if (in >> c && c != e.expected) {
      in.setstate(std::ios::failbit);
    }
    return in;
  }
};

struct PasswordRecord {
  size_t first_num;
  size_t second_num;
  char required_letter;
  std::string password;

  friend std::istream& operator>>(std::istream& in, PasswordRecord& record) {
    ExpectedChar hyphen{'-'};
    ExpectedChar colon{':'};
    in >> record.first_num >> hyphen >> record.second_num >>
        record.required_letter >> colon >> record.password;
    return in;
  }

  friend std::ostream& operator<<(std::ostream& os,
                                  const PasswordRecord& record) {
    os << record.first_num << '-' << record.second_num << ' '
       << record.required_letter << ": " << record.password;
    return os;
  }
};

// Treat first_num as the minimum allowed number of occurrences and second_num
// as the maximum allowed number of occurrences of required_letter to appear in
// the password.
bool IsPasswordRecordValidCount(const PasswordRecord& record) {
  size_t count = 0;
  for (char letter : record.password) {
    if (letter == record.required_letter) {
      count++;
    }
  }
  LOG(INFO) << "Found " << count << " occurrences of " << record.required_letter
            << " in \'" << record << "\"";
  return record.first_num <= count && count <= record.second_num;
}

// Treat first_num as the first position and second_num as the second position;
// required_letter must occur in exactly one of these positions.
bool IsPasswordRecordValidXor(const PasswordRecord& record) {
  bool pos1_check =
      (record.password.at(record.first_num - 1) == record.required_letter);
  bool pos2_check =
      (record.password.at(record.second_num - 1) == record.required_letter);
  return (pos1_check && !pos2_check) || (!pos1_check && pos2_check);
}

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day 2", argc, argv);

  std::string policy = absl::GetFlag(FLAGS_policy);
  LOG(INFO) << "Using policy \"" << policy << "\"";

  size_t valid_record_count = 0;
  PasswordRecord temp_record;
  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  while (file_stream >> temp_record) {
    LOG(INFO) << "Testing \"" << temp_record << "\"";
    if ((policy == "count" && IsPasswordRecordValidCount(temp_record)) ||
        (policy == "xor" && IsPasswordRecordValidXor(temp_record))) {
      valid_record_count++;
    }
  }

  std::cout << valid_record_count << std::endl;
}
