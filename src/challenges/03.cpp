#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "absl/flags/flag.h"
#include "absl/logging/logging.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");
ABSL_FLAG(int, horizontal, 1, "horizontal step distance");
ABSL_FLAG(int, vertical, 1, "vertical step distance");

struct Position {
  int x;
  int y;
};

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day 3", argc, argv);

  std::vector<std::vector<int>> map;

  std::string row;
  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  while (file_stream >> row) {
    std::vector<int> map_row;
    for (char c : row) {
      switch (c) {
        case '.':
          map_row.push_back(0);
          break;
        case '#':
          map_row.push_back(1);
        default:
          break;
      }
    }
    map.push_back(map_row);
  }

  Position slope = {.x = absl::GetFlag(FLAGS_horizontal),
                    .y = absl::GetFlag(FLAGS_vertical)};
  Position currentPosition = slope;

  int totalCount = 0;
  while (currentPosition.y < map.size()) {
    totalCount += map[currentPosition.y][currentPosition.x];

    currentPosition.x = (currentPosition.x + slope.x) % map[0].size();
    currentPosition.y += slope.y;
  }
  std::cout << "Trees Encountered: " << totalCount << std::endl;
}
