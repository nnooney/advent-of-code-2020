#include <cctype>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "absl/algorithm/container.h"
#include "absl/flags/flag.h"
#include "absl/logging/log_container.h"
#include "absl/logging/logging.h"
#include "absl/strings/numbers.h"
#include "absl/strings/str_split.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");
ABSL_FLAG(bool, validateFields, false, "if true, validate content of fields");

struct Passport {
  std::string birthYear;
  std::string issueYear;
  std::string expirationYear;
  std::string height;
  std::string hairColor;
  std::string eyeColor;
  std::string passportId;
  std::string countryId;
};

bool ValidateInt(std::string_view input, int min, int max) {
  int value;
  if (!absl::SimpleAtoi(input, &value)) {
    LOG(INFO) << "Error validating value: absl::SimpleAtoi returned false";
    return false;
  }

  if (min > value) {
    LOG(INFO) << "value " << value << " is greater than minimum value " << min;
    return false;
  }

  if (max < value) {
    LOG(INFO) << "value " << value << " is greater than maximum value " << max;
    return false;
  }

  return true;
}

bool ValidateHeight(std::string_view input) {
  if (input.ends_with("cm")) {
    input.remove_suffix(2);
    return ValidateInt(input, 150, 193);
  } else if (input.ends_with("in")) {
    input.remove_suffix(2);
    return ValidateInt(input, 59, 76);
  } else {
    LOG(INFO) << "Height does not end with 'cm' or 'in'";
    return false;
  }
}

bool ValidateHairColor(std::string_view input) {
  if (input.size() != 7) {
    LOG(INFO) << "Hair Color is wrong input length; expected 7, got "
              << input.size();
    return false;
  }

  if (!input.starts_with("#")) {
    LOG(INFO) << "Hair Color does not start with '#'";
    return false;
  }
  input.remove_prefix(1);

  for (char c : input) {
    if (!std::isxdigit(c)) {
      LOG(INFO) << "Hair Color contains non-hexadecimal digit '" << c << "'";
      return false;
    }
  }

  return true;
}

bool ValidateEyeColor(std::string_view input) {
  std::array<std::string, 7> validEyeColors = {"amb", "blu", "brn", "gry",
                                               "grn", "hzl", "oth"};
  if (absl::c_none_of(validEyeColors,
                      [=](std::string s) { return s == input; })) {
    LOG(INFO) << "Eye color " << input
              << " is not in allowlist: " << absl::LogContainer(validEyeColors);
    return false;
  }

  return true;
}

bool ValidatePassportId(std::string_view input) {
  if (input.size() != 9) {
    LOG(INFO) << "Passport ID is wrong input length; expected 9, got "
              << input.size();
    return false;
  }

  for (char c : input) {
    if (!std::isdigit(c)) {
      LOG(INFO) << "Passport ID contains non-digit '" << c << "'";
      return false;
    }
  }

  return true;
}

bool IsPassportValid(const struct Passport& p) {
  bool validateFields = absl::GetFlag(FLAGS_validateFields);

  if (p.birthYear.empty()) {
    LOG(INFO) << "Passport is missing required birth year";
    return false;
  }
  if (validateFields && !ValidateInt(p.birthYear, 1920, 2002)) {
    LOG(INFO) << "Passport contains invalid birth year";
    return false;
  }

  if (p.issueYear.empty()) {
    LOG(INFO) << "Passport is missing required issue year";
    return false;
  }
  if (validateFields && !ValidateInt(p.issueYear, 2010, 2020)) {
    LOG(INFO) << "Passport contains invalid issue year";
    return false;
  }

  if (p.expirationYear.empty()) {
    LOG(INFO) << "Passport is missing required expiration year";
    return false;
  }
  if (validateFields && !ValidateInt(p.expirationYear, 2020, 2030)) {
    LOG(INFO) << "Passport contains invalid expiration year";
    return false;
  }

  if (p.height.empty()) {
    LOG(INFO) << "Passport is missing required height";
    return false;
  }
  if (validateFields && !ValidateHeight(p.height)) {
    LOG(INFO) << "Passport contains invalid height";
    return false;
  }

  if (p.hairColor.empty()) {
    LOG(INFO) << "Passport is missing required hair color";
    return false;
  }
  if (validateFields && !ValidateHairColor(p.hairColor)) {
    LOG(INFO) << "Passport contains invalid hair color";
    return false;
  }

  if (p.eyeColor.empty()) {
    LOG(INFO) << "Passport is missing required eye color";
    return false;
  }
  if (validateFields && !ValidateEyeColor(p.eyeColor)) {
    LOG(INFO) << "Passport contains invalid eye color";
    return false;
  }

  if (p.passportId.empty()) {
    LOG(INFO) << "Passport is missing required passport ID";
    return false;
  }
  if (validateFields && !ValidatePassportId(p.passportId)) {
    LOG(INFO) << "Passport contains invalid passport ID";
    return false;
  }

  LOG(INFO) << "Passport is valid";
  return true;
}

void ProcessPassportInput(struct Passport& p, std::istringstream& line_stream) {
  std::string keyValuePair;
  while (line_stream >> keyValuePair) {
    std::vector<std::string> components = absl::StrSplit(keyValuePair, ':');

    if (components[0] == "byr") {
      p.birthYear = components[1];
    } else if (components[0] == "iyr") {
      p.issueYear = components[1];
    } else if (components[0] == "eyr") {
      p.expirationYear = components[1];
    } else if (components[0] == "hgt") {
      p.height = components[1];
    } else if (components[0] == "hcl") {
      p.hairColor = components[1];
    } else if (components[0] == "ecl") {
      p.eyeColor = components[1];
    } else if (components[0] == "pid") {
      p.passportId = components[1];
    } else if (components[0] == "cid") {
      p.countryId = components[1];
    } else {
      LOG(ERROR) << "Unrecognized input: \"" << components[0] << "\"";
    }
  }
}

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day 4", argc, argv);

  struct Passport passport;
  int validPassports = 0;

  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  std::string line;
  while (std::getline(file_stream, line)) {
    LOG(INFO) << "Processing line: " << line;
    std::istringstream line_stream(line);
    if (!line.empty()) {
      ProcessPassportInput(passport, line_stream);
    } else {
      LOG(INFO) << "Checking passport validity";
      if (IsPassportValid(passport)) {
        validPassports++;
      }
      passport = {};
    }
  }

  // Check the last passport in the input
  LOG(INFO) << "Checking final passport validity";
  if (IsPassportValid(passport)) {
    validPassports++;
  }

  std::cout << "Valid Passports: " << validPassports << std::endl;

  return 0;
}
