#include <bitset>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include "absl/algorithm/container.h"
#include "absl/flags/flag.h"
#include "absl/logging/logging.h"
#include "absl/strings/str_replace.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day 5", argc, argv);

  std::vector<unsigned long> seatIds;

  std::vector<std::pair<const std::string_view, std::string>> replacements = {
      {"B", "1"}, {"R", "1"}, {"L", "0"}, {"F", "0"}};

  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  std::string line;
  while (std::getline(file_stream, line)) {
    std::bitset<10> bits(absl::StrReplaceAll(line, replacements));
    seatIds.push_back(bits.to_ulong());
  }
  absl::c_sort(seatIds);

  for (int i = 1; i < seatIds.size(); ++i) {
    if (seatIds[i] != seatIds[i - 1] + 1) {
      std::cout << "Your Seat: " << seatIds[i] - 1 << std::endl;
    }
  }
  std::cout << "Max ID: " << seatIds.back() << std::endl;

  return 0;
}
