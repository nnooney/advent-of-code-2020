#include <fstream>
#include <iostream>
#include <string>

#include "absl/flags/flag.h"
#include "absl/logging/log_container.h"
#include "absl/logging/logging.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");
ABSL_FLAG(std::string, policy, "any", "policy to count questions (any, all)");

void ProcessQuestions(std::array<int, 26>& questions, std::string line) {
  for (char c : line) {
    if (c < 'a' || c > 'z') {
      LOG(INFO) << "Encountered invalid character " << c;
      continue;
    }
    questions[c - 'a'] += 1;
  }
}

int TallyQuestions(const std::array<int, 26>& questions, int members,
                   std::string policy) {
  LOG(INFO) << "Tallying " << absl::LogContainer(questions) << " by " << members
            << " members using policy " << policy;
  int sum = 0;
  for (int q : questions) {
    if (policy == "any" && q > 0) {
      sum += 1;
    } else if (policy == "all" && q == members) {
      sum += 1;
    }
  }
  return sum;
}

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day X", argc, argv);

  int totalSum = 0;
  std::string policy = absl::GetFlag(FLAGS_policy);
  int groupMembers = 0;
  std::array<int, 26> questions = {};

  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  std::string line;
  while (std::getline(file_stream, line)) {
    if (!line.empty()) {
      ProcessQuestions(questions, line);
      groupMembers++;
    } else {
      totalSum += TallyQuestions(questions, groupMembers, policy);
      groupMembers = 0;
      questions = {};
    }
  }

  totalSum += TallyQuestions(questions, groupMembers, policy);

  std::cout << "Total Questions Answered (" << policy << "): " << totalSum
            << std::endl;

  return 0;
}
