#include <fstream>
#include <iostream>
#include <string>

#include "absl/flags/flag.h"
#include "absl/logging/logging.h"
#include "init/init_google_lacuna.h"

ABSL_FLAG(std::string, input, "", "path to the input file");

int main(int argc, char** argv) {
  InitGoogleLacuna("The solution to day X", argc, argv);

  std::ifstream file_stream(absl::GetFlag(FLAGS_input));
  std::string line;
  while (std::getline(file_stream, line)) {
  }

  return 0;
}
